__fish_make_completion_signals

function ps_complete_pids
    #ps -eo pid,command | string match -r -v '^\s*'$fish_pid'\s' | tail -n +2 | string replace -r ' *([0-9]+) *$' '$1 [$2]' | string replace -r ' *\[\?*\] *$' ''
    ps -eo pid,command |tail -n +2 |string replace -r ' *([0-9]+) +([^ ].*[^ ]|[^ ])' '$1 [$2]' |string replace -r ' *\[\?*\] *$' ''
end
#complete -c pv -xa '(__fish_complete_pids)'
complete -c pv -xa '(ps_complete_pids)'

