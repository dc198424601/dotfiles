set -l commands diary zk add showinstall smalldpi animewall backuppkg imgdraw spcimglogo logo smalldpi_sway smbconfig
set -l wallpaper "-m print" "-m randomall" "-m randomstar" "-m add" "-m next" "-m back" "-m rd" "-m remove" "-m trim" "-m delete" "-m jump" "-m removekeep" "-m removestar" "-m printstar"
set zknotes "$HOME/Documents/Obsidian/Notes/ZK" 
set diaryDir "$HOME/Documents/Obsidian/Notes/Diary"
complete -c smallaids -f
complete -c smallaids -n 'not __fish_seen_subcommand_from $commands'  -a diary -f -d 'write diary'
complete -c smallaids -n 'not __fish_seen_subcommand_from $commands'  -a zk -f -d 'write zk notes' 
complete -c smallaids -n 'not __fish_seen_subcommand_from $commands'  -a add -d 'add video to animewall'
complete -c smallaids -n 'not __fish_seen_subcommand_from $commands'  -a showinstall -d 'show install informatin'
complete -c smallaids -n 'not __fish_seen_subcommand_from $commands'  -a smalldpi -d 'change to small dpi'
complete -c smallaids -n 'not __fish_seen_subcommand_from $commands'  -a smalldpi_sway -d 'change to small dpi'
complete -c smallaids -n 'not __fish_seen_subcommand_from $commands'  -a animewall -d 'open animewall'
complete -c smallaids -n 'not __fish_seen_subcommand_from $commands'  -a ndel -d 'ndel and translate'
complete -c smallaids -n 'not __fish_seen_subcommand_from $commands'  -a rm_snapthots_trash -d 'emtpy the snapshots trash'
complete -c smallaids -n 'not __fish_seen_subcommand_from $commands'  -a showTags -d 'show Notes Tags'
complete -c smallaids -n 'not __fish_seen_subcommand_from $commands'  -a wallpaper -d 'control wallpaper'
complete -c smallaids -n 'not __fish_seen_subcommand_from $commands'  -a backuppkg -d 'backuppkg pkg list'
complete -c smallaids -n 'not __fish_seen_subcommand_from $commands'  -a imgdraw -d 'convert clipboard image By 千玄子'
complete -c smallaids -n 'not __fish_seen_subcommand_from $commands'  -a spcimglogo -d 'logo $HOME/!temp/spc/'
complete -c smallaids -n 'not __fish_seen_subcommand_from $commands'  -a logo -d  'add logo'
complete -c smallaids -n 'not __fish_seen_subcommand_from $commands'  -a smbconfig -d  'add config to qemu/smb'
complete -c smallaids -n 'not __fish_seen_subcommand_from $commands'  -a do_waydroid -d  'do_waydroid'
complete -c smallaids -n '__fish_seen_subcommand_from add' -F
complete -c smallaids -n '__fish_seen_subcommand_from logo' -a "(ls)"
complete -c smallaids -n '__fish_seen_subcommand_from zk' -a "(ls $zknotes)"
complete -c smallaids -n '__fish_seen_subcommand_from diary' -a "(ls $diaryDir)"
complete -c smallaids -n '__fish_seen_subcommand_from wallpaper' -a "$wallpaper"
