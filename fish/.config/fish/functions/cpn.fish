function cpn --wraps=advcp --description 'alias cp advcp'
    if test (count $argv) -ge 2
        if not test -e (dirname $argv[-1])
            echo $argv[-1]
            echo (dirname $argv[-1]) not exist creating
            set newfile (dirname $argv[-1])
            if test (count $newfile) -eq 1
                mkdir -p $newfile
            end
        end
    end
    advcp $argv; 
end
