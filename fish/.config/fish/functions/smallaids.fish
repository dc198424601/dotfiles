function smallaids
    switch $argv[1]
        case add
            #调整大小并添加视频文件到animwall配置文件
            set a (ffprobe -v error -select_streams v:0 -show_entries stream=width,height -of default=noprint_wrappers=1:nokey=1  $argv[2])
            set w (echo $a|cut -d' ' -f1)
            set h (echo $a|cut -d' ' -f2)
            set k (math $h x 2256 / $w)
            set k (math floor $k)
            set p (math 1504-$k)
            set p (math floor (math $p / 2))
            set videopath (realpath $argv[2])
            set towrite "$videopath:2256x$k+0+$p:0"
            echo $towrite
            if grep --line-number $towrite $HOME/.config/animwall/animwall.conf
                echo 'Already init'
            else
                echo $towrite >$HOME/.config/animwall/animwall.conf
            end
        case zk
            if test $argv[2]
                nvim ~/Documents/Obsidian/Notes/ZK/$argv[2]
                cd ~/Documents/Obsidian/Notes/ZK
                $SHELL
            else
                fish $HOME/my_shell/typorashell/zknote vi
            end
        case diary
            if test $argv[2]
                if test -e ~/Documents/Obsidian/Notes/Diary/$argv[2]
                    nvim ~/Documents/Obsidian/Notes/Diary/$argv[2]
                else if test $argv[2] = ty
                    fish $HOME/my_shell/typorashell/diary ty
                else
                    fish $HOME/my_shell/typorashell/diary vi
                end
            else
                fish $HOME/my_shell/typorashell/diary vi
            end
        case showinstall
            expac  --timefmt='%Y-%m-%d %T' '%l\t%w\t%n' | sort | egrep 'explicit'|sort
            bash -c "expac -H M '%-20n\t%10d' \$(comm -23 <(pacman -Qqt | sort) <({ pacman -Qqg base-devel xorg gnome xfce4; expac -l '\n' '%E' base; } | sort -u))"
        case smalldpi
            xrandr --newmode "1152x768_60.00" 71.75 1152 1216 1328 1504 768 771 781 798 -hsync +vsync
            xrandr --addmode eDP1 1152x768_60.00
            xrandr -s 1152x768
            bash -c ~/.config/polybar/launch.sh
        case pac
            set pack (pacman -Qo "$argv[2]"|cut -d\   -f3)
            pactree -r $pack
            echo -------------------------------------------------------
            pacman -Qi $pack
        case animewall
            feh --bg-fill /home/with9/Image/black.jpg --no-fehbg
            /usr/bin/animwall
            ~/.fehbg
        case ndel
            cd $HOME/temp
            /home/with9/.Envs/sty/bin/python ~/my_shell/smalltools/delNshell.py $argv[2..-1]
        case rm_snapthots_trash
            for i in (ls $HOME/.snapshots/)
                echo $i
                sudo btrfs property set $HOME/.snapshots/{$i}/snapshot ro false
                /usr/bin/rm -rfv $HOME/.snapshots/{$i}/snapshot/.local/share/Trash
                sudo btrfs property set $HOME/.snapshots/{$i}/snapshot ro true
            end
		case showTags
			rg '^Tags' $HOME/Documents/Obsidian/Notes |rg '\[.+\]' -o|sed "s/[\"\']//g"|sed "s/\[//g"|sed "s/\]//g"|string split ,|sort|uniq
			echo '-------------------------------------'
			rg '^categories' $HOME/Documents/Obsidian/Notes |rg '\[.+\]' -o|sed "s/[\"\']//g"|sed "s/\[//g"|sed "s/\]//g"|string split ,|sort|uniq
		case wallpaper
			python $HOME/my_shell/wallpaperDown/wallpaper.py $argv[2..-1]
        case backuppkg
            pacman -Qenq;echo --------------------------;pacman -Qemq
            pacman -Qenq>~/dotfiles/pkglist
            pacman -Qemq>~/dotfiles/aurlist
        case imgdraw
           xclip -selection clipboard -t image/png -o > /tmp/src.png
           convert /tmp/src.png \
	            \( +clone -alpha extract \
	            -draw 'fill black polygon 0,0 0,8 8,0 fill white circle 8,8 8,0' \
	            \( +clone -flip \) -compose Multiply -composite \
	            \( +clone -flop \) -compose Multiply -composite \
                \) -alpha off -compose CopyOpacity -composite /tmp/output.png
            convert /tmp/output.png -bordercolor none -border 20 \( +clone -background black -shadow 100x15+0+0 \) \
	            +swap -background white -layers merge +repage /tmp/des.png
            composite -gravity SouthEast ~/Image/logo.png /tmp/des.png /tmp/des.png
            copyq write image/png - < /tmp/des.png
        case wimgdraw
           wl-paste > /tmp/src.png
           convert /tmp/src.png \
	            \( +clone -alpha extract \
	            -draw 'fill black polygon 0,0 0,8 8,0 fill white circle 8,8 8,0' \
	            \( +clone -flip \) -compose Multiply -composite \
	            \( +clone -flop \) -compose Multiply -composite \
                \) -alpha off -compose CopyOpacity -composite /tmp/output.png
            convert /tmp/output.png -bordercolor none -border 20 \( +clone -background black -shadow 100x15+0+0 \) \
	            +swap -background white -layers merge +repage /tmp/des.png
            composite -gravity SouthEast ~/Image/logo.png /tmp/des.png /tmp/des.png
            copyq write image/png - < /tmp/des.png
        case spcimglogo
            cd ~/!temp/spc
            if not test -d logo
                mkdir logo
            end
            for i in (ls)
                if not test -f  logo/$i; and test -f $i
                    convert $i \
                        \( +clone -alpha extract \
                        -draw 'fill black polygon 0,0 0,8 8,0 fill white circle 8,8 8,0' \
                        \( +clone -flip \) -compose Multiply -composite \
                        \( +clone -flop \) -compose Multiply -composite \
                        \) -alpha off -compose CopyOpacity -composite logo/$i
                    convert logo/$i -bordercolor none -border 20 \( +clone -background black -shadow 100x15+0+0 \) \
                        +swap -background white -layers merge +repage logo/$i
                    composite -gravity SouthEast ~/Image/logo.png logo/$i logo/$i                                                       
                    notify-send $i
                 end
            end
        case logo
           set filename (echo $argv[2]|cut -d. -f1)
           convert $argv[2] \
	            \( +clone -alpha extract \
	            -draw 'fill black polygon 0,0 0,8 8,0 fill white circle 8,8 8,0' \
	            \( +clone -flip \) -compose Multiply -composite \
	            \( +clone -flop \) -compose Multiply -composite \
                \) -alpha off -compose CopyOpacity -composite "$filename"_logo.jpg
            convert "$filename"_logo.jpg -bordercolor none -border 20 \( +clone -background black -shadow 100x15+0+0 \) \
	            +swap -background white -layers merge +repage "$filename"_logo.jpg
            composite -gravity SouthEast ~/Image/logo.png "$filename"_logo.jpg "$filename"_logo.jpg
        case smalldpi_sway
            swaymsg -- output eDP-1 mode --custom 1128x752
        case smbconfig
            for i in /tmp/qemu-smb*
sudo echo "[global]
allow insecure wide links = yes
[qemu]
follow symlinks = yes
wide links = yes
acl allow execute always = yes" >> sudo $i/smb.conf
            end
        case do_waydroid
            #sudo modprobe binder_linux devices=binder,hwbinder,vndbinder,anbox-binder,anbox-hwbinder,anbox-vndbinder
            #sudo modprobe ashmem_linux
            sudo systemctl start waydroid-container.service
            XDG_SESSION_TYPE=wayland weston
        case help
            echo 'add       :add video to animwall'
            echo 'zk        :make zk note'
            echo 'diary     :make diary'
            echo 'pac       :show package info'
            echo 'smalldpi  :small dpi'
            echo 'animewall :animwall show'
    end
end
