function musicbox
    /usr/bin/musicbox
    cd $HOME/.local/share/netease-musicbox
    if test -e database.json
        cp database.json database.jsonB
        jq -M '.collections |= unique_by(.song_id)' database.jsonB > database.json
        cat database.json |jq ".collections|.[]|.song_name"
    end
end
