function clean_mpv
for i in (ls $HOME/.config/mpv/saved_playlist/)
if grep $i $HOME/.config/mpv/history.log >/dev/null
continue
else
rm $HOME/.config/mpv/saved_playlist/$i
end
end
end
