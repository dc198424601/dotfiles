# Defined via `source`
function nmcliw --wraps='nmcli device wifi' --description 'alias nmcliw nmcli device wifi'
  nmcli device wifi $argv; 
end
