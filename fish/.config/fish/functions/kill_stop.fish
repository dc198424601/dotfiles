function kill_stop
    set ppid (xprop |grep "_NET_WM_PID(CARDINAL)"|cut -d= -f2)
    set pid_name (ps -p (string trim $ppid) -o comm|tail -1)
    echo $ppid
    if test $argv[1] = 'stop'
        kill -STOP $ppid
        notify-send "stop $pid_name"
    else if test $argv[1] = 'cont'
        kill -CONT $ppid
        notify-send "continue $pid_name"
    else if test $argv[1] = 'sleep'
        set sleep_min (zenity --title min --text inputMin  --entry 2)
        sleep (math floor (math $sleep_min x 60))
        notify-send "$sleep_min min over,will stop $pid_name"
        sleep 5
        kill -STOP $ppid
    end
end
