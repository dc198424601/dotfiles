#function i3_jump_to_free
set i3msg (i3-msg -t get_workspaces|jq '.[].num')
for i in (seq 30)
    if not contains $i $i3msg && test $i -ne 8
        i3-msg -t command workspace $i
        break
    end
end
