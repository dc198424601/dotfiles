# Defined in - @ line 1
function setproxy --wraps='export ALL_PROXY=socks5://127.0.0.1:8889' --description 'alias setproxy export ALL_PROXY=socks5://127.0.0.1:8889'
    export http_proxy=http://127.0.0.1:1089
    export https_proxy=$http_proxy
    export HTTP_PROXY=$http_proxy
    export HTTPS_PROXY=$http_proxy
    echo now in proxy
    fish
    echo exiting proxy
    set -e https_proxy
    set -e http_proxy
    set -e HTTP_PROXY
    set -e HTTPS_PROXY
end
