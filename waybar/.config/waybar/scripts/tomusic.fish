set music (playerctl -p netease-cloud-music,mpv,%any metadata|cut -d' ' -f1|uniq)
echo $music
if test $music  = 'netease-cloud-music'
    echo hi
    set music  '— Cloud Music'
end
set id (swaymsg -t get_tree|jq -r '.nodes[].nodes[] | if .nodes then [recurse(.nodes[])]
else [] end + .floating_nodes |  .[]  |  select(.nodes==[])  |
    ((.id | tostring) + "" + .name)'|grep -i -- $music|grep -o "^[0-9]\+")
echo $id[1]

swaymsg "[con_id=$id[1]]" focus
