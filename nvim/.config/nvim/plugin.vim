set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'itchyny/lightline.vim'
Plugin 'aliva/vim-fish',{'for':'fish'}
Plugin 'ferrine/md-img-paste.vim',{'for':'md'}
Plugin 'iamcco/markdown-preview.nvim',{'for':'md'}
let g:mdip_imgdir_absolute= '/home/with9/Documents/Obsidian/assets/.vimImg'
autocmd FileType markdown nmap <buffer><silent> <leader>i :call mdip#MarkdownClipboardImage()<CR>
Plugin 'godlygeek/tabular',{'for':'md'}
Plugin 'dhruvasagar/vim-table-mode',{'for':'md'}
Plugin 'dkarter/bullets.vim',{'for':'md'}
"Plugin 'git://git.wincent.com/command-t.git'
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
Plugin 'roxma/nvim-yarp'
Plugin 'ncm2/ncm2'
Plugin 'ncm2/ncm2-path'
Plugin 'ncm2/ncm2-bufword'
Plugin 'ncm2/ncm2-jedi',{'for':'python'}
Plugin 'ncm2/ncm2-vim',{'for':'vim'}
Plugin 'ncm2/ncm2-ultisnips'
"set completeopt=menu,noinsert
set completeopt=noinsert,menuone
inoremap <expr> <Tab> pumvisible() ? "\<C-y>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
autocmd BufEnter * call ncm2#enable_for_buffer()
Plugin 'APZelos/blamer.nvim',{'on':'Blamer'}
let g:blamer_enabled = 1
let g:blamer_delay=500
let g:blamer_prefix = '   >'
let g:blamer_date_format = '%y-%m-%d'
Plugin 'scrooloose/nerdtree',{'on':'NERDTree'}
noremap <leader>d :NERDTree<CR>
Plugin 'skywind3000/asyncrun.vim',{'on':'AsyncRun'}
let g:asyncrun_open = 6
noremap <leader>t :AsyncRun -mode=term
Plugin 'ap/vim-css-color',{'for':'css'}
Plugin 'Chiel92/vim-autoformat',{'on':'Autoformat'}
let g:autoformat_verbosemode=1
Plugin 'qpkorr/vim-renamer',{'on':'Renamer'}
Plugin 'sirver/ultisnips'
call vundle#end() " required
filetype plugin indent on " required
function! ZoteroCite()
  " pick a format based on the filetype (customize at will)
  let format = &filetype =~ '.*tex' ? 'citep' : 'pandoc'
  let api_call = 'http://127.0.0.1:23119/better-bibtex/cayw?format=pandoc&brackets=1'
  let ref = system('curl -s '.shellescape(api_call))
  return ref
endfunction

function! ZoteroMsg()
  " pick a format based on the filetype (customize at will)
  let format = &filetype =~ '.*tex' ? 'citep' : 'pandoc'
  let api_call = 'http://127.0.0.1:23119/better-bibtex/cayw?format=formatted-bibliography'
  let ref = system('curl -s '.shellescape(api_call))
  return ref
endfunction

  " pick a format based on the filetype (customize at will)
noremap <leader>z "=ZoteroCite()<CR>p
noremap <leader>t "=ZoteroMsg()<CR>p
inoremap <C-z> <C-r>=ZoteroCite()<CR>
autocmd BufEnter *.md  call tablemode#Enable()
if has("autocmd")
	au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
	\| exe "normal g'\"" | endif
endif

function Fcitx2en()
  if g:enable_fx==1
	  let inputstatus = system("fcitx5-remote")
	  if inputstatus == 2
			let b:inputtoggle = 1
			let t = system("fcitx5-remote -c")
	  endif
  endif
endfunction
function Fcitx2zh()
  if g:enable_fx==1
	  try
		if b:inputtoggle == 1
		  let t = system("fcitx5-remote -o")
		  let b:inputtoggle = 0
		endif
	  catch /inputtoggle/
		let b:inputtoggle = 0
	  endtry
  endif
endfunction

let enable_fx=0
autocmd BufEnter *.md let enable_fx=1
au InsertLeave * call Fcitx2en()
au InsertEnter * call Fcitx2zh()


let g:UltiSnipsExpandTrigger="<c-e>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
let g:UltiSnipsSnippetDirectories=[$HOME.'/.config/nvim/Ultisnips/']
