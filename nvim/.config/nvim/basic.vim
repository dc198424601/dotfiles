set nocompatible " be iMproved, required
set mouse=a
syntax on
set encoding=utf-8 
set fileencodings=ucs-bom,utf-8,sjis,gbk,default	
set laststatus=2 "lightline
set ts=4
set shiftwidth=4
filetype off " required
imap <C-S>  <ESC> :w <CR>
nmap <C-S> :w<CR>

function Mychinesecount()
	%s/[\u4E00-\u9FCC]//ng
endfunction

autocmd BufEnter PKGBUILD  set filetype=sh
highlight DiffAdd ctermbg=235  ctermfg=108  guibg=#262626 guifg=#87af87 cterm=reverse gui=reverse
highlight DiffDelete ctermbg=235  ctermfg=131  guibg=#262626 guifg=#af5f5f cterm=reverse gui=reverse
highlight DiffChange ctermbg=235  ctermfg=103  guibg=#262626 guifg=#8787af cterm=reverse gui=reverse
highlight DiffText ctermbg=235  ctermfg=208  guibg=#262626 guifg=#ff8700 cterm=reverse gui=reverse

nnoremap <S-K> :m-2<CR>
nnoremap <S-J> :m+<CR>
