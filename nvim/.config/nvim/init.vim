set nocompatible " be iMproved, required
"set mouse=a
syntax on
set number
highlight clear LineNr
highlight LineNr  ctermfg=DarkGrey guifg=DarkGrey
set laststatus=2 "lightline
set ts=4
set shiftwidth=4
set encoding=utf-8 
set fileencodings=ucs-bom,utf-8,sjis,gbk,default	
set expandtab
filetype off " required
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'itchyny/lightline.vim'
Plugin 'aliva/vim-fish',{'for':'fish'}
Plugin 'ferrine/md-img-paste.vim',{'for':'md'}
Plugin 'iamcco/markdown-preview.nvim',{'for':'md'}
let g:mdip_imgdir_absolute= '/home/with9/Documents/Obsidian/assets/.vimImg'
autocmd FileType markdown nmap <buffer><silent> <leader>i :call mdip#MarkdownClipboardImage()<CR>
Plugin 'godlygeek/tabular',{'for':'md'}
Plugin 'dhruvasagar/vim-table-mode',{'for':'md'}
autocmd BufEnter *.md  call tablemode#Enable()
Plugin 'dkarter/bullets.vim',{'for':'md'}
"Plugin 'git://git.wincent.com/command-t.git'
Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
Plugin 'roxma/nvim-yarp'
Plugin 'ncm2/ncm2'
Plugin 'ncm2/ncm2-path'
Plugin 'ncm2/ncm2-bufword'
Plugin 'ncm2/ncm2-jedi',{'for':'python'}
Plugin 'ncm2/ncm2-vim',{'for':'vim'}
Plugin 'ncm2/ncm2-ultisnips'
Plugin 'ncm2/ncm2-tern',{'for':'javascript'}
set completeopt=menu,noinsert
set completeopt=noinsert,menuone
inoremap <expr> <Tab> pumvisible() ? "\<C-y>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
autocmd BufEnter * call ncm2#enable_for_buffer()
"Plugin 'APZelos/blamer.nvim',{'on':'Blamer'}
let g:blamer_enabled = 1
let g:blamer_delay=500
let g:blamer_prefix = '   >'
let g:blamer_date_format = '%y-%m-%d'
Plugin 'scrooloose/nerdtree',{'on':'NERDTree'}
noremap <leader>d :NERDTree<CR>
Plugin 'skywind3000/asyncrun.vim',{'on':'AsyncRun'}
let g:asyncrun_open = 6
noremap <leader>t :AsyncRun -mode=term
"Plugin 'ap/vim-css-color'
Plugin 'Chiel92/vim-autoformat',{'on':'Autoformat'}
let g:autoformat_verbosemode=1
Plugin 'qpkorr/vim-renamer',{'on':'Renamer'}
Plugin 'sirver/ultisnips'
call vundle#end() " required
"filetype plugin indent on " required
function! ZoteroCite()
  " pick a format based on the filetype (customize at will)
  let format = &filetype =~ '.*tex' ? 'citep' : 'pandoc'
  let api_call = 'http://127.0.0.1:23119/better-bibtex/cayw?format=pandoc&brackets=1'
  let ref = system('curl -s '.shellescape(api_call))
  return ref
endfunction

function! ZoteroMsg()
  " pick a format based on the filetype (customize at will)
  let format = &filetype =~ '.*tex' ? 'citep' : 'pandoc'
  let api_call = 'http://127.0.0.1:23119/better-bibtex/cayw?format=formatted-bibliography'
  let ref = system('curl -s '.shellescape(api_call))
  return ref
endfunction
"
"  " pick a format based on the filetype (customize at will)
noremap <leader>z "=ZoteroCite()<CR>p
noremap <leader>t "=ZoteroMsg()<CR>p
"inoremap <C-z> <C-r>=ZoteroCite()<CR>
if has("autocmd")
	au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
	\| exe "normal g'\"" | endif
endif
"
"au BufRead,BufNewFile * let b:save_time = localtime()
"let g:autosave_time = 600
" save if needed / update the save_time after the save
"function! UpdateFile()
"  if((localtime() - b:save_time) >= g:autosave_time)
"      update
"      let b:save_time = localtime()
"	  echo 'auto save'
  "else
      " just debugging info
      "echo \"[+] \". (localtime() - b:save_time) ." seconds have elapsed so far."
"  endif
"endfunction

"au FocusLost * call UpdateFile()
imap <C-S>  <ESC> :w <CR>
nmap <C-S> :w<CR>
imap <C-D>  <ESC> :q! <CR>
nmap <C-D> :q!<CR>
nnoremap <S-K> :m-2<CR>
nnoremap <S-J> :m+<CR>
imap <C-N>  <ESC> :tab_new <CR>
nmap <C-N> :tab_new<CR>
nnoremap _ :set wrap! <CR> 
"
function Mychinesecount()
	%s/[\u4E00-\u9FCC]//ng
endfunction
"
"
autocmd BufEnter PKGBUILD  set filetype=sh
autocmd BufEnter *.fish  set filetype=fish
autocmd BufEnter *.md set shiftwidth=2
highlight DiffAdd ctermbg=235  ctermfg=108  guibg=#262626 guifg=#87af87 cterm=reverse gui=reverse
highlight DiffDelete ctermbg=235  ctermfg=131  guibg=#262626 guifg=#af5f5f cterm=reverse gui=reverse
highlight DiffChange ctermbg=235  ctermfg=103  guibg=#262626 guifg=#8787af cterm=reverse gui=reverse
highlight DiffText ctermbg=235  ctermfg=208  guibg=#262626 guifg=#ff8700 cterm=reverse gui=reverse
set listchars=space:␣,tab:»\ 
"
"
function Fcitx2en()
  if g:enable_fx==1
	  let inputstatus = system("fcitx5-remote")
	  if inputstatus == 2
			let b:inputtoggle = 1
			let t = system("fcitx5-remote -c")
	  endif
  endif
endfunction
function Fcitx2zh()
  if g:enable_fx==1
	  try
		if b:inputtoggle == 1
		  let t = system("fcitx5-remote -o")
		  let b:inputtoggle = 0
		endif
	  catch /inputtoggle/
		let b:inputtoggle = 0
	  endtry
  endif
endfunction

let enable_fx=0
autocmd BufEnter *.md let enable_fx=1
"au InsertLeave * call Fcitx2en()
"au InsertEnter * call Fcitx2zh()


"! ultisnips


" Trigger configuration. You need to change this to something other than <tab> if you use one of the following:
" - https://github.com/Valloric/YouCompleteMe
" - https://github.com/nvim-lua/completion-nvim
let g:UltiSnipsExpandTrigger="<c-e>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
let g:UltiSnipsSnippetDirectories=[$HOME.'/.config/nvim/Ultisnips/']

" mytab
nmap <leader>r :< <CR>
nmap <leader>t :> <CR>
