import pyperclip
import time
import os
import re
import sys
from itranslate import itranslate as itrans

def judge(oldString):
    flag=0 #超过三个汉字则返回false
    symbol_flag=0
    for ch in oldString:
            if u'\u4e00' <= ch <= u'\u9fff':
                flag=flag+1
            if ch=='/':
                symbol_flag=symbol_flag+1
            if flag>=3 or symbol_flag>=3:
                return False
    if 'http' ==oldString[:4] or 'www'==oldString[:3]or '.com'==oldString[-4:]:
        return False
    return True


if __name__=='__main__':
    os.chdir(os.path.expanduser('~/temp'))
    print('we are now in clip del mode')
    history_file=os.path.expanduser('~')+'/my_shell/smalltools/del_history'
    pyperclip.copy('start')
    del_space_flag=0
    if sys.argv[1]=='1':
        del_space_flag=1
        os.system("notify-send 'del_space on'")
    to_keep_s=''
    while 1:
        f=open(history_file,'a')
        time.sleep(0.5)
        oldString=pyperclip.paste()
        if oldString=='start' or oldString==to_keep_s:
            continue
        if '\n' in oldString:
            if del_space_flag:
                newString=oldString.replace('\r','').replace("-\n",'').replace("\n",' ')
            else:
                newString=oldString.replace('\r','').replace("-\n",'').replace("\n",'')
        else:
            newString=oldString
        newString=re.sub(r"\[\d+\]","",newString)#替换引用文献部分
        newString=re.sub(r"\(.*?\)","",newString)#替换括号部分
        newString=re.sub(r"\[.*?\]","",newString)#替换括号部分
        print("---------------------------")
        print(newString)
        pyperclip.copy(newString)
        try:
            trans=itrans(newString)
            print(trans)
        except:
            trans='something wrong,cant get trans'
            pass
        to_keep_s=newString
        f.write("{}\n{}\n---------------------\n".format(newString,trans))
        if 'END'==oldString[:3]:
            os.system('notify-send delN Stop')
            break
        if 'del_space'==oldString:
            del_space_flag=1-del_space_flag
            os.system("notify-send \"taggle del_space_flag {}\"".format(str(del_space_flag)))
            pyperclip.copy(str(del_space_flag))
        f.close()

