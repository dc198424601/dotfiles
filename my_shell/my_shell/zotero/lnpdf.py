import os
import sys
zoterdir=os.path.expanduser('~')+"/Zotero/storage"
os.chdir(zoterdir)
fl=[]
sfl=[]
pdf_symlink_file=os.path.expanduser('~')+'/Zotero/pdflnf'
for p,d,file in os.walk(os.getcwd()):
    for f in file:
        if f.endswith(".pdf"):
            fl.append(os.path.join(p,f))
            sfl.append(os.path.join(pdf_symlink_file,f))

for i in range(len(fl)):
    if os.path.exists(sfl[i]):
        continue
    else:
        os.symlink(fl[i],sfl[i])
        print("%s 连接成功" %sfl[i])

