var items = Zotero.getActiveZoteroPane().getSelectedItems();
var autors=[];
// return items
for(item of items){
    var a =item.getCreators()
    for(author of a){
        autors.push(author.lastName)
        item.addTag(author.lastName)
    }
    await item.saveTx()
}
return autors
