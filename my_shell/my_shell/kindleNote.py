import sys
import os
clipfile=sys.argv[1]
outdir=sys.argv[2]

if not os.path.exists(outdir):
    os.mkdir(outdir)




with open(clipfile)as f:
    all_list=[]#一个存放列表的列表,其中子列表内容为===之间的行
    store_list=[]
    for line in f:
        if line=='==========\n':
            if store_list:
                all_list+=[store_list]
                store_list=[]
        else:
            store_list.append(line)

book_dict={}
for one_mark in all_list:
    if (n := len(one_mark))>=3:
        if "Highlight" in one_mark[1] or "标注" in one_mark[1]:
            key=one_mark[0].replace('\n','')[:50]
            book_dict.setdefault(key,[]).append(f"{one_mark[1]}")
            for i in one_mark[2:]:
                if i !="\n":
                    book_dict[key].append(f">{i}")
                else:
                    book_dict[key].append(f"{i}")


for key,item in book_dict.items():
    fileout=f"{outdir}/{key}.md"
    #文件已经存在仅追加新内容
    if os.path.exists(fileout):
        old_lines=open(fileout,'r').readlines()
        with open(fileout,'a')as f:
            f.write('\n')
            f.writelines(list(filter(lambda i: i !=">\n" and i not in old_lines,item)))
    else:
        with open(fileout,'w')as f:
            f.write(f"## {key}\n")
            f.writelines(list(filter(lambda i: i !=">\n",item)))

#os.system(f"cat {outdir}/* |egrep -v '^-' >{outdir}/all.md")
