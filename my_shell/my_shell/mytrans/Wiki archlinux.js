{
	"translatorID": "ecc73d8a-2a85-4b99-9321-8bcd883446f0",
	"label": "Wiki archlinux",
	"creator": "with",
	"target": "https://wiki.archlinux.(org|jp)/",
	"minVersion": "3.0",
	"maxVersion": "",
	"priority": 100,
	"inRepository": true,
	"translatorType": 4,
	"browserSupport": "gcs",
	"lastUpdated": "2021-11-18 02:44:39"
}

/*
	***** BEGIN LICENSE BLOCK *****

	Copyright © 2020 YOUR_NAME <- TODO
	
	This file is part of Zotero.

	Zotero is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	Zotero is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with Zotero. If not, see <http://www.gnu.org/licenses/>.

	***** END LICENSE BLOCK *****
*/

function detectWeb(doc, url) {
	// TODO: adjust the logic here
	if (url.includes("wiki")) {
		return "encyclopediaArticle";
	} else if (getSearchResults(doc, true)) {
		return "multiple";
	}
	return false;
}

function getSearchResults(doc, checkOnly) {
	var items = {};
	var found = false;
	// TODO: adjust the CSS selector
	var rows = doc.querySelectorAll('h2>a.title[href*="/article/"]');
	for (let row of rows) {
		// TODO: check and maybe adjust
		let href = row.href;
		// TODO: check and maybe adjust
		let title = ZU.trimInternal(row.textContent);
		if (!href || !title) continue;
		if (checkOnly) return true;
		found = true;
		items[href] = title;
	}
	return found ? items : false;
}

function doWeb(doc, url) {
	if (detectWeb(doc, url) == "multiple") {
		Zotero.selectItems(getSearchResults(doc, false), function (items) {
			if (items) ZU.processDocuments(Object.keys(items), scrape);
		});
	} else {
		scrape(doc, url);
	}
}
function scrape(doc, url) {
	const newItem = new Zotero.Item("encyclopediaArticle");
	const title = doc.title;
	if(title.includes('简体中文')){
		newItem.language='zh';
	}
	else if (doc.getElementById('firstHeading').lang=='ja'){
		newItem.language='ja';
	}
	else{
		newItem.language='en';
	}
	newItem.url = url;
	newItem.title = title;
	newItem.abstractNote = doc.getElementsByTagName("p")[1].textContent;
	newItem.encyclopediaTitle = "ArchWiki";
	newItem.libraryCatalog = "ArchWiki";
	newItem.attachments = [
		{
			url: url,
			title: title,
			mimeType: "text/html",
			snapshot: true,
		},
	];
	newItem.complete();
}
