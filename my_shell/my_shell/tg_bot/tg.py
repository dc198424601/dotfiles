import logging
from telegram import Update
import os
from telegram.ext import filters, MessageHandler, ApplicationBuilder, CommandHandler, ContextTypes

# 日志记录
logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=update.effective_chat.id, text="I'm a bot, please talk to me!")

async def echo(update: Update, context: ContextTypes.DEFAULT_TYPE):
    out_text=f"{update.message.text}!{update.message.text}!{update.message.text}!"
    await context.bot.send_message(chat_id=update.effective_chat.id, text=out_text)

async def run(update: Update, context: ContextTypes.DEFAULT_TYPE):
    if update.message.from_user.id == 660274903:
        try:
            out_text=os.popen("fish -c '"+' '.join(context.args)+"'").read()
            out_text=f"```\n{out_text}\n```"
            await context.bot.send_message(chat_id=update.effective_chat.id, text=out_text,parse_mode="Markdown")
        except:
            await context.bot.send_message(chat_id=update.effective_chat.id, text='cmd not allowed or output too long')
    else:
        await context.bot.send_message(chat_id=update.effective_chat.id, text='not allowed')

async def caps(update: Update, context: ContextTypes.DEFAULT_TYPE):
    text_caps = ' '.join(context.args).upper()
    await context.bot.send_message(chat_id=update.effective_chat.id, text=text_caps)


if __name__ == '__main__':
    application = ApplicationBuilder().token(os.getenv('TG_TOKEN')).build()

    echo_handler = MessageHandler(filters.TEXT & (~filters.COMMAND), echo)
    start_handler = CommandHandler('start', start)
    run_handler = CommandHandler('run', run)
    caps_handler = CommandHandler('caps', caps)

    application.add_handler(start_handler)
    application.add_handler(echo_handler)
    application.add_handler(run_handler)
    application.add_handler(caps_handler)
    
    application.run_polling()
