import time
import pickle
import os
import sys
import random
import argparse
import requests
import shutil
import threading
from bs4 import BeautifulSoup
import hashlib
FOLDER = os.path.expanduser('~/Image/Wallpapers')


def isLock():
    if not os.path.exists(FOLDER):
        return False
    LOCK = os.path.join(FOLDER, '.lock')
    if os.path.exists(LOCK) and (time.time()-os.path.getctime(LOCK) > 600):
        os.remove(LOCK)
    if os.path.exists(LOCK):
        return True
    else:
        with open(LOCK, "w")as f:
            f.write(str(time.time()))
        return False


def unlock():
    LOCK = os.path.join(FOLDER, '.lock')
    if os.path.exists(LOCK):
        os.remove(LOCK)


class Wallpaper:
    def __init__(self):
        self.locktime = time.time()
        self.folder = FOLDER
        self.file = os.path.join(self.folder, '.wallpaper_class')
        self.onehaven_folder = os.path.join(self.folder, 'one_haven')  # wallhaven 缓存图片
        self.homewallpaper = os.path.expanduser("~/.wallpaper")
        self.picture_name = ''  # 当前壁纸
        self.locked = False
        self.wallpaper_list = []
        self.temp_wallpaper_list = []
        self.onehaven_img = []
        self.count = 0  # 统计get值用于判断是否需要生成新的onehavenlist
        self.index = -1  # 目前在列表中的索引位置
        self.waring = 0  # 数量过多警告
        self.keep = []
        self.star = []
        self.mkfolders()
        self.keep_init()

    def mkfolders(self):
        # 初始化创建文件夹
        for i in [self.folder, self.onehaven_folder, os.path.join(self.folder, 'star')]:
            if not os.path.exists(i):
                os.makedirs(i)
                print(f'mkdir {i}')

    def file_name(self, file_dir):
        # 获取路径下文件
        for root, dirs, files in os.walk(file_dir):
            return [root, dirs, files]
    def check_wm(self):
        wm=os.popen("wmctrl -m|grep Name").read()
        if 'GNOME' in wm.upper():
            return 'gnome'
        elif 'I3' in wm.upper():
            return 'i3'
        elif os.getenv("SWAYSOCK"):
            return 'sway'
        return 'i3'
        
    def set_wallpaper(self):
        self.back_to_normal_index()
        if self.wallpaper_list:
            self.picture_name = self.wallpaper_list[self.index]
            while not os.path.exists(self.picture_name) and self.wallpaper_list:
                print(f'removing {self.picture_name}')
                self.remove_img()
                self.back_to_normal_index()
                self.picture_name = self.wallpaper_list[self.index] if self.wallpaper_list else self.picture_name

        if self.wallpaper_list:
            if (wm := self.check_wm())=='gnome':
                os.system(f"gsettings set org.gnome.desktop.background picture-uri 'file:///{self.picture_name}'")
            elif wm=='i3':
                os.system(f"feh --bg-fill \"{self.picture_name}\"")
            elif wm=='sway':
                os.system(f"swaymsg -t command output \* bg {self.picture_name} fill")
            self.md5 = hashlib.md5(open(self.picture_name, 'rb').read()).hexdigest()
        else:
            print('no image in list')

    def save(self):
        # 将实例化的对象保存在本地
        f = open(self.file, 'wb')
        pickle.dump(self, f)
        f.close()

    def judge_img(self, picture):
        # 判断当前文件是否是图片
        if os.path.islink(picture):
            picture = os.path.realpath(picture)
        if 'image' in os.popen(f"file -i '{picture}'").read().split(':')[-1]:
            return True
        return False

    def randomall(self, submode='all'):
        # 从wallpapers文件夹随机一张图片作为壁纸
        if submode == 'star':
            if not self.star:
                return
            picture_name = random.choice(self.star)
        else:
            if not self.keep:
                return
            picture_name = random.choice(self.keep)
        self.wallpaper_list.insert(self.index+1, picture_name)
        self.index = self.index+1
        self.set_wallpaper()

    def back(self):
        # 列表上一张作为图片
        self.index = self.index-1
        if self.index <= -1:
            self.index = (len(self.wallpaper_list)-1)
        self.set_wallpaper()

    def next(self):
        # 列表下一张作为图片
        self.index = self.index+1
        if self.index >= len(self.wallpaper_list):
            self.index = 0
        self.set_wallpaper()

    def get_one_page(self, url_old, page):
        url = f"{url_old}{page}"
        req = requests.get(url)
        soup = BeautifulSoup(req.text)
        self.onehaven_img += list(map(lambda i: i['href'],
                                  soup.findAll(class_='preview')))
        print(len(self.onehaven_img))

    def get_wallhaven_list(self, submode='110'):
        # 获取wallhaven.cc的图片列表
        url_old = f"https://wallhaven.cc/search?categories={submode}&purity=100&topRange=1M&sorting=toplist&order=desc&page="
        try:
            requests.get("https://www.bing.com")
        except:
            print('no internet')
            unlock()
            sys.exit()
        if self.count == 0:
            self.onehaven_img = []
            for page in range(1, 40):
                self.get_one_page(url_old, page)

            print(f"oneheaven list:{len(self.onehaven_img)}")
            self.count = 1

    def get_one(self):
        # 获取一张haven图片作为壁纸
        self.get_wallhaven_list()
        self.picture_name = self.download_one(random.choice(self.onehaven_img))
        self.wallpaper_list.insert(self.index+1, self.picture_name)
        self.index = self.index+1

    def keep_init(self):
        keep_folder = self.folder
        star_folder = os.path.join(self.folder, 'star')

        self.keep = list(filter(self.judge_img, map(
            lambda x: os.path.join(self.folder, x), self.file_name(self.folder)[2])))
        self.star = list(filter(self.judge_img, map(
            lambda x: os.path.join(star_folder, x), self.file_name(star_folder)[2])))

    def keep_tofolder(self, submode=None):
        self.keep = list(set(self.keep))
        self.star = list(set(self.star))
        to_keep_name = self.md5
        new_name = os.path.join(self.folder, to_keep_name)
        if os.path.exists(self.picture_name):
            try:
                print(f"{self.picture_name}------->{new_name}")
                os.system(f"cp '{self.picture_name}' {new_name}")
                self.wallpaper_list = [
                    f"{new_name}" if name == self.picture_name else name for name in self.wallpaper_list]
                self.keep.append(new_name)
            except:
                pass
        if submode == 'star':
            if os.path.exists(new_name):
                try:
                    new_link_name = os.path.join(
                        self.folder, 'star', to_keep_name)
                    print(f"{new_name}------->{new_link_name}")
                    os.system(f"cp {new_name} {new_link_name}")
                    self.wallpaper_list = [
                        f"{new_link_name}" if name == new_name else name for name in self.wallpaper_list]
                    self.keep.append(new_link_name)
                    self.star.append(new_link_name)
                except:
                    pass

    def trim(self,trim_len):
        try:
            trim_len=int(trim_len)
        except:
            print('please input int')
            return
        if self.index < trim_len//2:
            self.wallpaper_list = self.wallpaper_list[:trim_len+10]
        elif (len(self.wallpaper_list)-self.index) < trim_len//2:
            old_len = len(self.wallpaper_list)
            self.wallpaper_list = self.wallpaper_list[-trim_len:]
            self.index = self.index-(old_len-trim_len)
        else:
            self.wallpaper_list = self.wallpaper_list[self.index-trim_len//2:self.index+trim_len//2+1]
            self.index = trim_len//2

    def to_home(self):
        # 复制图片到$HOME/.wallpaper,并设定权限
        if self.picture_name and self.wallpaper_list:
            if os.path.exists(self.homewallpaper):
                os.remove(self.homewallpaper)
            shutil.copy(self.picture_name, self.homewallpaper)
            os.system('chmod 666 {}'.format(self.homewallpaper))

    def clear(self):
        self.wallpaper_list = []
        self.index = -1

    def remove_img(self, submode='soft'):
        # 从列表删除该图片
        if not self.wallpaper_list:
            unlock()
            sys.exit()
        del self.wallpaper_list[self.index]
        if submode == 'hard':
            if self.picture_name in self.keep or self.picture_name in self.star:
                os.system(f"notify-send in_keep")
            else:
                os.remove(self.picture_name)  # 同时删除文件 
                if self.onehaven_folder in self.picture_name: 
                    os.system(f"touch {self.picture_name}") # 创建空白文件
                self.wallpaper_list = list(
                    filter(lambda i: i != self.picture_name, self.wallpaper_list))  # 从列表里面全部删除
                self.keep = list(
                    filter(lambda i: i != self.picture_name, self.keep))  # 从列表里面全部删除
                self.star = list(
                    filter(lambda i: i != self.picture_name, self.star))  # 从列表里面全部删除
                self.wallpaper_list = list(
                    filter(self.judge_img, self.wallpaper_list)) # 排除非图片
        if self.wallpaper_list:
            self.set_wallpaper()
        else:
            self.clear()

    def add_one(self, file=None):
        file = file.replace('\n', '')
        if not file:
            print('You should select one picture')
        elif not self.judge_img(file):
            print('The file looks like not image file')
        else:
            self.wallpaper_list.insert(self.index+1, file)
            self.index = self.index+1

    def add_dir(self, dir):
        pictures = self.file_name(dir)[2]
        pictures = list(map(lambda i: os.path.join(dir, i), pictures))
        pictures = list(filter(self.judge_img, pictures))
        self.wallpaper_list = self.wallpaper_list+pictures
        print(f"append:{len(pictures)}")

    def download_one(self, url):
        # 从haven下载一张图片
        i = 0
        while 1:
            try:
                i = i+1
                req = requests.get(url)
                soup = BeautifulSoup(req.text, 'lxml')
                img_tag = soup.find(id='wallpaper')
                img_url = img_tag['src']
                filename = img_url.split('/')[-1]
                filename = os.path.join(self.onehaven_folder, filename)
                if os.path.exists(filename):
                    print('exist')
                    self.temp_wallpaper_list.append(filename)
                    return filename
                os.system(f"wget {img_url} -O {filename}")
                self.temp_wallpaper_list.append(filename)
                return filename
                break
            except:
                #             print('{}not ok'.format(url))
                continue
                if i >= 10:
                    break

    def remove_duplicate(self):
        # 去重
        if self.wallpaper_list:
            self.wallpaper_list = list(set(self.wallpaper_list))
            self.wallpaper_list = list(
                filter(self.judge_img, self.wallpaper_list))
            self.index = self.wallpaper_list.index(self.picture_name)
            print(f'len:{len(self.wallpaper_list)}')

    def print(self, submode=None):
        # 显示一些基本内容
        if submode != 'star':
            for i in range(len(self.wallpaper_list)):
                print("{}\t{}".format(i, self.wallpaper_list[i]))
        else:
            print("--------------collect---------------")
            for i in range(len(self.keep)):
                print("{:<10d}{}".format(i, self.keep[i]))
            print("--------------star---------------")
            for i in range(len(self.star)):
                print("{:<10d}{}".format(i, self.star[i]))
        print("now\t{}\nindex:{}\ncount:{}".format(
            self.picture_name, self.index, self.count))

    def back_to_normal_index(self):
        if self.index >= len(self.wallpaper_list):
            self.index = len(self.wallpaper_list)-1
        if self.index < 0:
            self.index = 0


if __name__ == '__main__':
    if isLock():
        os.system("notify-send Locked!")
        sys.exit()

    wallpaper_file = os.path.join(FOLDER, '.wallpaper_class')
    if os.path.exists(wallpaper_file):
        f = open(wallpaper_file, 'rb')
        wallpaper = pickle.load(f)
        f.close()
    else:
        wallpaper = Wallpaper()
        wallpaper.save()
        wallpaper.get_one()
    parser = argparse.ArgumentParser()
    parser.add_argument("-m", "--mode", default='123')
    parser.add_argument("-s", "--submode", default='110')
    args = parser.parse_args()
    # args.mode='next'
    if args.mode == 'randomall':
        wallpaper.randomall()
    elif args.mode == 'back':
        wallpaper.back()
    elif args.mode == 'next':
        wallpaper.next()
    elif args.mode == 'get':
        keep_index = wallpaper.index+1
        if args.submode:
            try:
                rounds = 1 if args.submode == '110' else int(args.submode)
            except:
                rounds = 1
        for i in range(rounds):
            wallpaper.get_one()
            wallpaper.count = wallpaper.count+1
        if wallpaper.count >= 1000:
            wallpaper.count = 0
        wallpaper.index = keep_index
        wallpaper.set_wallpaper()
    elif args.mode == 'randomstar':
        wallpaper.randomall('star')
    elif args.mode == 'keep':
        wallpaper.keep_tofolder()
        wallpaper.set_wallpaper()
    elif args.mode == 'star':
        wallpaper.keep_tofolder('star')
        wallpaper.set_wallpaper()
    elif args.mode == 'add':
        path = os.path.realpath(args.submode)
        if os.path.isdir(path):
            wallpaper.add_dir(path)
        else:
            wallpaper.add_one(path)
            wallpaper.set_wallpaper()
    elif args.mode == 'addfile':
        imgfile = os.path.realpath(args.submode)
        temp_count = 0
        for i in open(imgfile, 'r').readlines():
            wallpaper.add_one(os.path.realpath(i.replace('\n', '')))
            temp_count += 1
        wallpaper.index -= temp_count
        wallpaper.set_wallpaper()

    elif args.mode == 'rebuild':
        wallpaper.count = 0
        wallpaper.get_wallhaven_list(args.submode)
    elif args.mode == 'print':
        wallpaper.print()
        unlock()
        sys.exit()
    elif args.mode == 'show':
        os.system(f"notify-send -r 1048 -i {wallpaper.picture_name} {wallpaper.index}/{len(wallpaper.wallpaper_list)} \"{wallpaper.picture_name}\nkeep:{wallpaper.picture_name in wallpaper.keep};star:{wallpaper.picture_name in wallpaper.star}\"")
        unlock()
        sys.exit()
    elif args.mode == 'printstar':
        wallpaper.print('star')
        unlock()
        sys.exit()
    elif args.mode == 'remove':
        wallpaper.remove_img()
    elif args.mode == 'delete':
        wallpaper.remove_img('hard')
    elif args.mode == 'keepinit':
        wallpaper.keep_init()
    elif args.mode == 'rd':
        wallpaper.remove_duplicate()
        wallpaper.keep_init()
    elif args.mode == 'clear':
        wallpaper.clear()
    elif args.mode == 'trim':
        wallpaper.trim(args.submode)
    elif args.mode == 'jump':
        try:
            wallpaper.index = int(args.submode)
            wallpaper.picture_name = wallpaper.wallpaper_list[wallpaper.index]
            wallpaper.set_wallpaper()
        except:
            print("please input correct index")
            unlock()
            sys.exit()

    else:
        msg = '''
        randomall     :random one file from keep
        back|next     :back &&next
        keep|star     :collect |star
        get           :get one picture from wallhaven
        add           :add one file or dir to list
        addfile       :add from file containing img list
        remove|delete :remove from list|delete file
        rd            :remove duplicate
        print         :print basic message
        rebuild xxx   :rebuild the haven list xxx can  110(default) 1:genery 2:anime 3:people    
        clear         :clear img_list
        trim          :trim to 300
        keepinit      :init wallpaper.keep

        '''
        print(msg)

    print("{:<10d}{}".format(wallpaper.index, wallpaper.picture_name))
    wallpaper.to_home()
    wallpaper.locked = False
    if len(wallpaper.wallpaper_list) >= 1000:
        wallpaper.waring = wallpaper.waring+1
        if wallpaper.waring >= 50:
            os.system('notify-send \'need to trim or rd\'')
            wallpaper.waring = 0
    wallpaper.save()
    os.system(f"notify-send -r 1048 -i {wallpaper.picture_name} {wallpaper.index}/{len(wallpaper.wallpaper_list)} \"{wallpaper.picture_name}\nkeep:{wallpaper.picture_name in wallpaper.keep or wallpaper.md5 in os.listdir(wallpaper.folder)};star:{wallpaper.picture_name in wallpaper.star or wallpaper.md5 in os.listdir(os.path.join(wallpaper.folder,'star'))}\"")
    unlock()
