---
title: nedd_install 
date: 2021-11-04
author: with
bibliography: /home/with9/bib/my.bib
csl: /home/with9/bib/nature.csl
tag: #note 
---

- alacritty
- *aria2c*
- *dunst*
- feh
- fish
- adobe-source-han-sans-cn-fonts
- adobe-source-han-sans-otc-fonts
- adobe-source-han-serif-otc-fonts
- adobe-source-code-pro-fonts
- i3-gaps
- rofi
- *dmenu*
- picom
- *libnotify*
- ranger
- *thunar*
- *mpd*
- *skippy-xd*
- mpv
- fcitx5-im
- fcitx5-rime
- [archlinucn](Server = https://mirrors.tuna.tsinghua.edu.cn/archlinuxcn/$arch)
	- *fontweak*
	- *ruby-fusuma*
	- polybar
	- *espanso*

```shell
sudo pacman -Syu sddm stow neovim alsa-utils otf-font-awesome
sudo pacman -Syu alacritty feh adobe-source-han-sans-otc-fonts adobe-source-han-serif-otc-fonts adobe-source-code-pro-fonts fontweak polybar rofi dmenu picom mpv fcitx5-im fcitx5-rime
```
