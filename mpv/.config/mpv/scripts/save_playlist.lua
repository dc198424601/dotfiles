local playlist_savepath = (os.getenv('APPDATA') or os.getenv('HOME')..'/.config')..'/mpv'..'/saved_playlist/'

local utils = require("mp.utils")
local msg = require("mp.msg")

local filename = nil

local log_file = os.getenv('HOME')..'/.config'..'/mpv'..'/history.log'

--saves the current playlist into a m3u file


function save_playlist()
  local length = mp.get_property_number('playlist-count', 0)
	if length == 0 then return end
  local title = mp.get_property("media-title"):gsub("\"", "")
  local cmd=string.format("zenity --title 保存列表名称 --text 输入  --width 500 --entry --entry-text='%s'_L%s.m3u",title,length)
  local handle = io.popen(cmd)
  local result = handle:read()
  handle:close()
  local savepath = utils.join_path(playlist_savepath, result)
  local file, err = io.open(savepath, "w")
  if not file then
	msg.error("Error in creating playlist file, check permissions and paths: "..(err or ""))
  else
	local i=0
	while i < length do
	  local pwd = mp.get_property("working-directory")
	  local filename = mp.get_property('playlist/'..i..'/filename')
	  local fullpath = filename
	  if not filename:match("^%a%a+:%/%/") then
		fullpath = utils.join_path(pwd, filename)
	  end
	  file:write(fullpath, "\n")
	  i=i+1
	end
	msg.info("Playlist written to: "..savepath)
	mp.osd_message("Playlist written to: "..savepath)
	file:close()
  end
  -- save to recent log
  if not savepath then return end
  -- if in exist
  f = io.open(log_file, "a+")
  f:write(("[%s] \"%s\" | %s\n"):format(os.date("%d/%m/%y %X"), title, savepath))
  f:close()
  --
end

mp.add_key_binding("b", save_playlist)
-- https://www.reddit.com/r/mpv/comments/ax925a/playlist_how_to_save_currently_internal_playlist/
