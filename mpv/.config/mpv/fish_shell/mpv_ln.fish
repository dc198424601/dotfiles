if test $PWD = '/home/with9'
    test -d $HOME/Videos/!star || mkdir $HOME/Videos/!star
    set filepath (realpath $argv[2])
    set lnname (md5sum $filepath|cut -f1 -d' ')
    ln -sr "$filepath" $HOME/Videos/!star/$lnname
    notify-send link "$filepath --> $HOME/Videos/!star/$lnname"
    
else
    test -d $PWD/!star || mkdir !star
    set filepath (realpath $argv[2])
    set lnname (md5sum $filepath|cut -f1 -d' ')
    ln -sr "$filepath" !star/$lnname
    notify-send link "$filepath --> $PWD/!star/$lnname"
end
