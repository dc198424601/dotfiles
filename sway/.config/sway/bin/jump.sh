#!/bin/sh
wofi --show dmenu -I -G| {
           read -r id name
           i=`echo $id|grep -o '^[0-9]\+'`
           echo $i
           swaymsg "[con_id=$i]" focus
       }
