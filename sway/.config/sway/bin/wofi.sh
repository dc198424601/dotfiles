#!/bin/sh
swaymsg -t get_tree |
         jq -r '.nodes[].nodes[] | if .nodes then [recurse(.nodes[])]
       else [] end + .floating_nodes |  .[]  |  select(.nodes==[])  |
       ((.id | tostring) + "" + .name)' |grep -v '^[0-9]\+$'|grep -v 'i3_scratch'|
         wofi --show dmenu,drun -I -G | {
           read -r id name
           i=`echo $id|grep -o '^[0-9]\+'`
           echo $i
           swaymsg "[con_id=$i]" focus
       }
