#i3-msg -t command workspace 6
set playerctl_env $(cat $HOME/.config/polybar/scripts/playerctl_env)
set old_msg (playerctl -p $playerctl_env metadata|tail -1)
playerctl -p $playerctl_env next
set new_msg (playerctl -p $playerctl_env metadata|tail -1)
while test $old_msg = $new_msg
    sleep 0.5
    set new_msg (playerctl -p $playerctl_env metadata|tail -1)
end
notify-send -r 10 (playerctl -p $playerctl_env metadata     --format '{{artist}}-{{title}}-{{album}}')
