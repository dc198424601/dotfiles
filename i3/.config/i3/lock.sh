#!/usr/bin/fish

if upower -i /org/freedesktop/UPower/devices/battery_BAT1 | grep "state" | grep -q "discharging"
    systemctl hibernate -i
else
    set BLANK '#00000000'
    set CLEAR '#ffffff22'
    set DEFAULT '#66ccff'
    set TEXT '#66ccff'
    set WRONG '#ffffff'
    set VERIFYING '#66ccff'
    
    i3lock \
    --insidever-color=$CLEAR     \
    --ringver-color=$VERIFYING   \
    \
    --insidewrong-color=$CLEAR   \
    --ringwrong-color=$WRONG     \
    \
    --inside-color=$BLANK        \
    --ring-color=$DEFAULT        \
    --line-color=$BLANK          \
    --separator-color=$DEFAULT   \
    \
    --verif-color=$TEXT          \
    --wrong-color=$TEXT          \
    --time-color=$TEXT           \
    --date-color=$TEXT           \
    --layout-color=$TEXT         \
    --keyhl-color=$WRONG         \
    --bshl-color=$WRONG          \
    \
    --screen 1  \
    --blur 5 \
    --clock \
    --date-str="%A, %Y-%m-%d"
end

